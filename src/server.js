import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import morgon from 'morgan'

import tokenValidator from './middlewares/token-validator'

import {
    authRouter,
    commentRouter
} from './services/router'

class Server {
    app
    constructor() {
        this.app = express()
        this.config()
        this.routes()
    }

    config() {
        this.app.use(cors())
        this.app.use(bodyParser.urlencoded({
            extended: true
        }))
        this.app.use(bodyParser.json())
        this.app.use(morgon('combined'))
    }

    routes() {
        let router = express.Router()

        router.use('/auth', authRouter)
        router.use('/comment', tokenValidator(), commentRouter)

        this.app.use('/', router)

    }
}

export default new Server().app