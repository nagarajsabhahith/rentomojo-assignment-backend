import mongoose from 'mongoose'

const userSchema = mongoose.Schema({
    id:{
        type: String, // user id from github
        required: true
    },
    access_token: {
        type: String, // user access token from github
        required: true
    },
    alias: {
        type: String, // user alias name
        required: true
    },
    name: {
        type: String, // user full name
        required: true
    },
    email: {
        type: String, // user email address
        required: true
    },
    avatar: {
        type: String, // user profile image
        default: null,
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

const userModel = mongoose.model('users', userSchema, 'users')

export default userModel