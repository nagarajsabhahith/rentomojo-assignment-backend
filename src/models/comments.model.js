import mongoose from 'mongoose'

//Schema for the comments and replies
const commentsSchema = mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId, // user object id from user collection
        required: true,
        ref: 'users'
    },
    comment: {
        type: String,
        required: true // user comment will be stored here
    },
    parent_id: {
        type: mongoose.Schema.Types.ObjectId, // parent comment id
        default: null,
        ref: 'comments'
    },
}, {
    timestamps: {
        createdAt: 'created_at', // created date time
        updatedAt: 'updated_at' // updated date time
    }
})

const commentsModel = mongoose.model('comments', commentsSchema, 'comments')

export default commentsModel