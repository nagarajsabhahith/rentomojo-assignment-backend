import UserModel from './user.model'
import CommentsModel from './comments.model'

// All models in common file
export {
    UserModel,
    CommentsModel
}