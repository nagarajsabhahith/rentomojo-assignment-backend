import cluster from 'cluster'
import os from 'os'
import http from 'http'
import dotenv from 'dotenv'
import server from './server'
import mongoose from 'mongoose'
import {
    Timer
} from './utilities/utilities'

const CORES = os.cpus().length

// dotenv, It declares all variables from .env file for node process envoirnment
dotenv.config()

if (cluster.isMaster) {
    masterProcess()
} else {
    childProcess()
}

function masterProcess() {
    if (process.env.NODE_ENV == 'production') {
        console.log(`Master cluster setting up ${CORES} workers...`)
        for (let i = 0; i < CORES; i++) {
            cluster.fork()
        }
        cluster.on('online', function (worker) {
            // console.log(worker)
            console.log(`Worker started at ${Timer.getCurrentDateTime()}, PID: ${worker.process.pid} is online`)
        })
        cluster.on('exit', function (worker, code, signal) {
            console.log(`Worker died at ${Timer.getCurrentDateTime()}, PID: ${worker.process.pid} died with code: ${code}, and signal: ${signal}`)
            console.log(`Starting a new worker`)
            cluster.fork()
        })
    } else {
        childProcess()
    }
}

function childProcess() {
    const app = http.createServer(server)
    app.listen(process.env.PORT)
    app.on('error', error)
    app.on('listening', connected)
}


function connected() {
    console.log(`Server connected successfuly.`)
    mongoose.connect(process.env.MONGODB_URL, {
        useNewUrlParser: true,
        useCreateIndex: true,
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000,
        useFindAndModify: false,
    }).then((res) => {
        console.log(`Mongo DB connected successfuly.`)
        // console.log(res)
    }).catch(err => {
        console.log(`Database not conneted`)
        console.log(err)
    })
}

function error(error) {
    console.log(`Error!. Server not connected`)
    // console.log(error)

    if (error.syscall !== 'listen') {
        console.log(error)
    }
    switch (error.code) {
        case 'EADDRINUSE':
            console.log(`${ process.env.PORT } is already in use`)
            break
        default:
            throw error
    }
    process.exit(1)

}