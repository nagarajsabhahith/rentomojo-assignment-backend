import joi from 'joi'

// required params for adding and repling
const comment = joi.object().keys({
    comment: joi.string().required().label('Comment'),
    parent_id: joi.string().required().label('Parent ID').allow(null),
})

// required params for updating
const update = joi.object().keys({
    _id: joi.string().required().label('ID'),
    comment: joi.string().required().label('Comment'),
})

export default {
    comment,
    update
}