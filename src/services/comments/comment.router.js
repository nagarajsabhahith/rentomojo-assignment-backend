import express from 'express'
import ctrl from './comment.controller'
import params from './comment.params'
import paramsValidator from '../../middlewares/params-validator'

class CommentRouter {
    router
    constructor() {
        this.router = express.Router()
        this.routes()
    }

    routes() {
        this.router.post('/add', paramsValidator(params.comment), this.add)
        this.router.get('/list', this.list)
        this.router.post('/update', paramsValidator(params.update), this.update)
    }

    add(req, res) {
        ctrl.add(req.decoded, req.body).then(result => {
            res.status(result.status).json(result)
            res.end()
        }).catch(error => {
            console.log(error)
            res.status(error.status).json(error)
            res.end()
        })
    }

    list(req, res) {
        ctrl.list(req.decoded, req.query).then(result => {
            res.status(result.status).json(result)
            res.end()
        }).catch(error => {
            console.log(error)
            res.status(error.status).json(error)
            res.end()
        })
    }

    update(req, res) {
        ctrl.update(req.decoded, req.body).then(result => {
            res.status(result.status).json(result)
            res.end()
        }).catch(error => {
            console.log(error)
            res.status(error.status).json(error)
            res.end()
        })
    }
}

export default new CommentRouter().router