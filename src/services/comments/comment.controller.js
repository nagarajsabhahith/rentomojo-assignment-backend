import _ from 'underscore'
import {
    StatusCodes,
    Response
} from '../../utilities/utilities'
import {
    CommentsModel
} from '../../models/models'

class CommentController {

    constructor() {}

    // Adding new comment to system
    add(decoded, body) {
        return new Promise((resolve, reject) => {
            const comment = new CommentsModel({
                user_id: decoded._id,
                comment: body.comment,
                parent_id: body.parent_id
            })

            comment.save(err => {
                if (err) {
                    reject(Response.error(StatusCodes.internal_error))
                } else {
                    resolve(Response.success({
                        comment: comment
                    }, 'Comment added successfully'))
                }
            })
        })
    }

    // Updating comment to system
    update(decoded, body) {
        return new Promise((resolve, reject) => {
            CommentsModel.findByIdAndUpdate(body._id, {
                $set: {
                    comment: body.comment
                }
            }, (err, res) => {
                if (err) {
                    reject(Response.error(StatusCodes.internal_error))
                } else {
                    if (res) {
                        resolve(Response.success({
                            comment: res
                        }, 'Comment updated successfully'))
                    } else {
                        reject(Response.error(StatusCodes.not_found, {
                            key: '_id'
                        }, 'Comment not found'))
                    }
                }
            })
        })
    }

    // getting list of comments
    list(decoded, body) {
        return new Promise((resolve, reject) => {
            // Started with comments model to look up
            const aggregate = CommentsModel.aggregate()

            // finding user details from the user id
            aggregate.lookup({
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user'
            })

            // By default user will be array at this point of time, adding username field to result from the user array,
            // decoded object is having current user details, if the comment is from the same user can_edit will be true
            aggregate.addFields({
                user_name: {
                    $arrayElemAt: ['$user.name', 0]
                },
                can_edit: {
                    $cond: {
                        if: {
                            $eq: ["$user_id", decoded._id]
                        },
                        then: true,
                        else: false
                    }
                }
            })

            // Selecting out only required fields
            aggregate.project({
                _id: 1,
                parent_id: 1,
                user_id: 1,
                user_name: 1,
                comment: 1,
                updated_at: 1,
                can_edit: 1
            })
            aggregate.exec((err, comments) => {
                if (err) {
                    reject(Response.error(StatusCodes.internal_error))
                } else {

                    // Data tree operation is started here
                    // roots and children, if parent id is null then that comment is main comment
                    // parent id holds the reference id for comment
                    // if parent id is exists in the document then it is children 
                    let roots = {}
                    let childs = {}

                    // Separating roots and children from the result
                    for (const item in comments) {
                        if (!comments[item].parent_id) {
                            roots[item] = comments[item];
                        } else {
                            const parent_id = comments[item].parent_id;

                            childs[parent_id] = childs[parent_id] || [];
                            childs[parent_id].push(comments[item])
                        }
                    }

                    // Looping main comments to check is there any replies present in comment
                    for (const i in roots) {
                        tree(roots[i])
                    }

                    // each comment and checking their replies 
                    function tree(item) {
                        for (const subItem in childs[item._id]) {
                            let elem = childs[item._id][subItem];
                            // if already replies present the adding same replies or empty array will be added
                            item.replies = item.replies || [];
                            item.replies.push(elem);
                            // getting child replies
                            childs[elem._id] && tree(elem);
                        }
                    }

                    // making the final array of root comments
                    const result = []
                    for (const key in roots) {
                        if (roots.hasOwnProperty(key)) {
                            result.push(roots[key])
                        }
                    }

                    resolve(Response.success({
                        comments: result
                    }, 'Comment list'))
                }
            })
        })
    }


}

export default new CommentController()