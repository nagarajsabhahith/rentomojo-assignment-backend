import {
    StatusCodes,
    Response,
    JWT
} from '../../utilities/utilities'
import {
    UserModel
} from '../../models/models'

class AuthController {

    constructor() {}

    // Below login function is used for both login and register
    // If user is already present in db then simply generating access token from our system
    // If the user email is not exist in our db then adding user details to db and generating access token use other apis
    login(body) {
        return new Promise((resolve, reject) => {
            UserModel.findOne({
                email: body.email
            }, (err, user) => {
                if (err) {
                    reject(Response.error(StatusCodes.internal_error))
                } else {
                    if (user) {
                        resolve(Response.success({
                            token: JWT.generate({
                                user_id: user._id,
                                name: user.name,
                            }),
                            user: {
                                _id: user._id,
                                name: user.name,
                                email: user.email,
                                avatar: user.avatar
                            }
                        }, 'Login success'))

                    } else {
                        const newUser = new UserModel({
                            id: body.id,
                            name: body.name,
                            email: body.email,
                            alias: body.alias,
                            avatar: body.avatar,
                            access_token: body.access_token,
                        })
                        newUser.save(err => {
                            if (err) {
                                reject(Response.error(StatusCodes.internal_error))
                            } else {
                                resolve(Response.success({
                                    token: JWT.generate({
                                        user_id: newUser._id,
                                        name: newUser.name,
                                    }),
                                    user: {
                                        _id: newUser._id,
                                        name: newUser.name,
                                        email: newUser.email,
                                        avatar: newUser.avatar
                                    }
                                }, 'Login success'))
                            }
                        })
                    }
                }
            })
        })
    }


}

export default new AuthController()