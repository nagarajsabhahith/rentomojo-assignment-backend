import express from 'express'
import ctrl from './auth.controller'
import params from './auth.params'
import paramsValidator from '../../middlewares/params-validator'

class AuthRouter {
    router
    constructor() {
        this.router = express.Router()
        this.routes()
    }

    routes() {
        // Routes for login and regiter
        this.router.post('/login', paramsValidator(params.login), this.login)
    }

    login(req, res){
        ctrl.login(req.body).then(result => {
            res.status(result.status).json(result)
            res.end()
        }).catch(error => {
            console.log(error)
            res.status(error.status).json(error)
            res.end()
        })
    }
}

export default new AuthRouter().router