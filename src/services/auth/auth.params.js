import joi from 'joi'

// requred parameter to registartion and login which is coming from the github user data
const login = joi.object().keys({
    id: joi.string().required().label('ID'),
    name: joi.string().required().label('User Name'),
    alias: joi.string().required().label('Alias Name'),
    email: joi.string().required().label('Email'),
    avatar: joi.string().required().label('Avatar').allow(['', null]),
    access_token: joi.string().required().label('Access Token'),
})

export default {
    login,
}