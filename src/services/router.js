import authRouter from "./auth/auth.router"
import commentRouter from './comments/comment.router'

export {
    authRouter,
    commentRouter
}