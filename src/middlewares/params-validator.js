import joi from 'joi'
import {
    StatusCodes,
    Response
} from '../utilities/utilities'


// To validate request params, Validating with the help of joi plugin
export default function validator(params) {
    return function (req, res, next) {
        let data
        if (req.method === 'GET') {
            // if request method is get all the params will be in query 
            data = req.query
        } else if (req.method === 'POST') {
            // if request method is post all the params will be in body 
            data = req.body
        }
        if (data !== undefined && data !== null) {
            // validating with joi
            joi.validate(data, params, (err, value) => {
                if (err) {
                    res.status(StatusCodes.bad_request).json(Response.error(StatusCodes.bad_request, err.details[0].context, err.details[0].message))
                    res.end()
                } else {
                    // if required params are correct then passing it to next handler
                    next()
                }
            })
        } else {
            res.status(StatusCodes.bad_request).json(Response.error(StatusCodes.bad_request, {}, `Please provide all required fields!`))
            res.end()
        }
    }
}