
const codes = {
    success : 200, // Success
    no_content : 204, // if there is no content based of request data
    partial_content : 206, // if there is partial content based of request data
    bad_request : 400, // if required request data not present
    not_authorized : 401, // if auth expired
    forbidden : 403, // The server understood the request but refuses to authorize it.
    not_found : 404, // if there is no record
    not_acceptable : 406, //not accept this request
    conflict : 409, // if same data exists
    internal_error : 500 // internal server
}

export default codes