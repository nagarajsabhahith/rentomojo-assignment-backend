import jwt from 'jsonwebtoken'

const config = {
    secret: 'i love coding',
    expiry: 24 * 60 * 60 * 30 // 30 days
}

class JsonToken {
    constructor() {}

    generate(payload){
        const token = jwt.sign(payload, config.secret, {
            expiresIn: config.expiry
        })
        return token
    }

    verify(token){
        return new Promise((resolve, reject) => {
            jwt.verify(token, config.secret, (err, decoded) => {
                if(err){
                    console.log(err)
                    reject('error')
                }else {
                    resolve(decoded)
                }
                
            })
        })
    }


}

export default new JsonToken()