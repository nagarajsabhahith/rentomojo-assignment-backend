
class Response {
    constructor() {
    }

    success(body = {}, message = ``) {
        return {
            status: 200,
            message: message,
            body: body
        }
    }
    
    error(code, body = {}, message = `Something went wrong!`) {
        return {
            status: code,
            message: message,
            body: body
        }
    }

}

export default new Response()