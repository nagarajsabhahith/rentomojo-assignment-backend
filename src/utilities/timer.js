import moment from 'moment'

class Timer {
    constructor() {
    }

    getCurrentDateTime(){
        // 2018-06-23 14:04:59
        return moment().format('YYYY-MM-DD HH:mm:ss')
    }

    

}

export default new Timer()